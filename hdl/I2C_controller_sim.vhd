-- file I2C_controller_sim.vhd

-- the entity of a simulation environment usually has no input output ports.
-- file I2C_controller_sim_arc.vhd


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_unsigned.conv_integer;
use ieee.std_logic_arith.conv_std_logic_vector;


entity I2C_controller_sim is
  port(
	SCL: OUT STD_ULOGIC;
	SDA: INOUT STD_ULOGIC;
	Ack: OUT STD_ULOGIC;
	DataOut: OUT STD_ULOGIC_VECTOR(7 DOWNTO 0);
	StateOut: OUT STD_ULOGIC_VECTOR(7 DOWNTO 0);
	RegisterAddressOut: OUT STD_ULOGIC_VECTOR(7 DOWNTO 0)
);
end entity I2C_controller_sim;

architecture sim of I2C_controller_sim is

-- we declare signals to be connected to the instance of . the names of the
-- signals are the same as the name of the ports of the entity I2C_controller because it is
-- much simpler but we could use different names and bind signal names to port
-- names in the instanciation of I2C_controller.
  signal CLK_50_MHz, ResetN: STD_ULOGIC;
  signal Read_WriteN: STD_ULOGIC;
  signal DataIn, AddressIn: STD_ULOGIC_VECTOR(7 DOWNTO 0);
  signal stop_simulation: bit;
  CONSTANT max200k: INTEGER := 50000000/(100000*2);	-- to get 100kHz for I2C standard; every 2 cycles = 1 I2C cycle
--	CONSTANT max200k: INTEGER := 50000000/4;	-- for some I2C slaves (Maxim DS1375 RTC) the actual clock speed can be any speed less than the max I2C clock speed
	-- some I2C slaves (Maxim DS3232 RTC) requires a minimum SCL rate, so when this clock is too slow, the slave will reset the I2C interface
  SIGNAL clockticks200k: INTEGER RANGE 0 TO max200k;
  SIGNAL CLK_200k_Hz: STD_ULOGIC;
  SIGNAL bitcount: INTEGER RANGE 0 TO 7;
  SIGNAL bitcount1: INTEGER RANGE 0 TO 7;



begin

-- this process generates a symmetrical clock with a period of 20 ns.
-- this clock will never stop.
  clock_generator: process
  begin
    CLK_50_MHz <= '0';
    wait for 10 ns;
    CLK_50_MHz <= '1';
    wait for 10 ns;
    if stop_simulation = '1' then
      wait;
    end if;
  end process clock_generator;

  clockdivider200k: PROCESS
	BEGIN
		WAIT UNTIL CLK_50_MHz'EVENT and CLK_50_MHz = '1';
		IF clockticks200k < max200k THEN
			clockticks200k <= clockticks200k + 1;
		ELSE
			clockticks200k <= 0;
		END IF;
		IF clockticks200k < max200k/2 THEN
			CLK_200k_Hz <= '0';
		ELSE
			CLK_200k_Hz <= '1';
		END IF;
	END PROCESS;

-- this process generates the input sequence for the signal Read_WriteN.
  Read_WriteN_generator: process
    type num_cycles_array is array(natural range 1 to 2) of positive;
    constant num_cycles: num_cycles_array := (57, 400);
  begin
    for i in num_cycles'range loop
      for j in 1 to num_cycles(i) loop
        wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      end loop;
      Read_WriteN <= '1';
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      Read_WriteN <= '0';
    end loop;
    if stop_simulation = '1' then
      wait;
    end if;
  end process Read_WriteN_generator;

-- this process generates the input sequence for the signal reset

  reset_generator: process
    type num_cycles_array is array(natural range 1 to 4) of positive;
    constant num_cycles: num_cycles_array := (1, 853, 1500, 2000);
  begin
    for i in num_cycles'range loop
      for j in 1 to num_cycles(i) loop
        wait until CLK_50_MHz = '0' and CLK_50_MHz'event;
      end loop;
      ResetN <= '0';
      wait until CLK_50_MHz = '0' and CLK_50_MHz'event;
      ResetN <= '1';
    end loop;

    report "End of simulation";
    stop_simulation <= '1';
  end process reset_generator;

-- this process generates the input sequence for the signal AddressIn.

  AddressIn_generator: process    
  begin
    for i in 1 to 11 loop
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      if (bitcount - 1 ) >= 0 THEN
        AddressIn(bitcount) <= '0';    
        bitcount <= bitcount - 1;
      end if;
      if i = 11 then
        bitcount <= 7;
      end if;
    end loop;
    for i in 12 to 19 loop
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      AddressIn(bitcount) <= '1';
      bitcount <= bitcount - 1;
      if i = 19 then
        bitcount <= 7;
      end if;
    end loop;
    for i in 20 to 47 loop
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      if (bitcount - 1 ) >= 0 THEN
        AddressIn(bitcount) <= '0';    
        bitcount <= bitcount - 1;
      end if;
      if i = 47 then
        bitcount <= 7;
      end if;
    end loop;
    for i in 48 to 55 loop
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      AddressIn(bitcount) <= '1';
      bitcount <= bitcount - 1;
    end loop;
    
    if stop_simulation = '1' then
      wait;
    end if;
  end process AddressIn_generator;

-- this process generates the input sequence for the signal DataIn.

  DataIn_generator: process
  begin
    for i in 1 to 21 loop
      wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      if (bitcount1 - 1 ) >= 0 THEN
        DataIn(bitcount1) <= '0';    
        bitcount1 <= bitcount1 - 1;
      end if;
      if i = 21 then
        bitcount1 <= 7;
      end if;
    end loop;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '1';
      bitcount1 <= bitcount1 - 1;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '0';
      bitcount1 <= bitcount1 - 1;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '1';
      bitcount1 <= bitcount1 - 1;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '0';
      bitcount1 <= bitcount1 - 1;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '1';
      bitcount1 <= bitcount1 - 1;
   
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '0';
      bitcount1 <= bitcount1 - 1;
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '1';
      bitcount1 <= bitcount1 - 1;
   
    wait until CLK_200k_Hz = '0' and CLK_200k_Hz'event;
      DataIn(bitcount1) <= '0';
      bitcount1 <= bitcount1 - 1;


    if stop_simulation = '1' then
      wait;
    end if;
  end process DataIn_generator;



-- we instanciate the entity I2C_controller, architecture arc. we name the instance i_I2C_controller and
-- specify the association between port names and actual signals.
  i_I2C_controller: entity work.I2C_controller(FSMD) port map(CLK_50_MHz => CLK_50_MHz, ResetN => ResetN, SCL => SCL, SDA => SDA, Read_WriteN => Read_WriteN, DataIn => DataIn, AddressIn => AddressIn, Ack => Ack, DataOut => DataOut, StateOut => StateOut, RegisterAddressOut => RegisterAddressOut);

end architecture sim;
