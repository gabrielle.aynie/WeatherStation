1) PORTS connect�s au PMOD JC (I2C signaux de contr�le) :

SCL: OUT STD_LOGIC, connect� � PIN 2 : JC1_N, d'apr�s l'architecure : IO_L10N_T1_34

SDA: INOUT STD_LOGIC, connect� � PIN 3 : JC2_P, d'apr�s l'architecture : IO_L1P_T0_34

GND connect� au GND

VCC connect� � VCC3V3

2) PORTS user Interface AXI (signaux des donn�es) : 

Read_WriteN: IN STD_LOGIC : 1 bit pour dire si on (le ma�tre) veut �crire ou lire sur le SDA (0 pour �crire, 1 pour lire)

DataIn: IN STD_LOGIC_VECTOR(7 DOWNTO 0) : data que le ma�tre veut mettre dans un certain registre de l'esclave.

AddressIn: IN STD_LOGIC_VECTOR(7 DOWNTO 0) : le ma�tre veut acc�der au registre poss�dant cette adresse.

Ack: OUT STD_LOGIC : signal qu'envoie l'esclave au ma�tre lorsqu'il a re�u les donn�es (ou vice versa), sur 1bit.

DataOut: OUT STD_LOGIC_VECTOR(7 DOWNTO 0) : donn�es que l'esclave renvoie au ma�tre.

StateOut: OUT STD_LOGIC_VECTOR(7 DOWNTO 0) : indique l'�tat de l'automate.

RegisterAddressOut: OUT STD_LOGIC_VECTOR(7 DOWNTO 0) : contient soit l'adresse du registre de l'esclave dont on voit acc�der ou soit l'�tat de l'automate. Si on a une erreur dans l'acknoledgment, on a l'�tat courant dans RegisterAddressOut.

3) PORTS pour reset et clock : 

CLK_50_MHz: IN STD_LOGIC : utilis� pour diviser le front d'horloge. Chaque 2 cycle correspond � 1 cycle I2C.

ResetN: IN STD_LOGIC : reset.

